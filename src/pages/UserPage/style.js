import styled from "styled-components";

export const UserInfo = styled.section`
  display: flex;
  justify-content: space-around;
  align-items: center;
  height: 150px;
  border-bottom: 1px solid lightgray;

  img {
    width: 100px;
    height: 100px;
    border-radius: 50%;
    box-shadow: 0 0 5px black;
  }
  span {
    color: rgb(36, 36, 36);
    font-weight: 500;
    font-size: 22px;
  }

  @media (min-width: 768px) {
    flex-direction: column;
    margin-top: 10px;
    border: 0;
  }
`;

export const Repos = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;

  h1 {
    color: rgb(36, 36, 36);
    align-self: center;
    font-size: 20px;
  }

  ul {
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 90%;
    a {
      width: 90%;
      border-radius: 5px;

      li {
        width: 90%;

        border: 2px inset rgb(75, 75, 75);
        margin: 5px 0;
        padding: 10px;
        border-radius: 5px;

        background-color: lightgrey;

        h3 {
          text-transform: lowercase;
          color: rgb(25, 25, 25);
        }

        p {
          font-size: 12px;
          color: rgb(55, 55, 55);
        }
      }
    }
  }

  @media (min-width: 768px) {
    h1 {
      margin: 15px 0;
      font-size: 24px;
    }

    ul {
      width: 800px;
      flex-direction: row;
      flex-wrap: wrap;
      justify-content: center;
      gap: 5px;

      a {
        width: 380px;

        li {
          width: 380px;
          margin: 0;
          height: 100px;

          &:hover {
            border: 2px outset rgb(75, 75, 75);
          }
        }

        &:hover {
          transform: scale(97%);
          transition: all 0.2s;
        }
      }
    }
  }
`;
