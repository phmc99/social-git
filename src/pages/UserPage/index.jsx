import { UserInfo, Repos } from "./style";
import { NavBar } from "../../components/navigation";

export const UserProfile = ({ userData, setSearchResult }) => {
  const userAvatar = userData[0].owner["avatar_url"];
  const userName = userData[0].owner["login"];
  return (
    <>
      <header>
        <NavBar setSearchResult={setSearchResult} />
      </header>
      <main>
        <UserInfo>
          <img src={userAvatar} alt="" />
          <span>@{userName}</span>
        </UserInfo>
        <Repos>
          <h1>Seus projetos</h1>
          <ul>
            {userData.map((item, index) => (
              <a
                key={index}
                href={item["html_url"]}
                target="_blank"
                rel="noreferrer"
              >
                <li>
                  <h3>{item.name}</h3>
                  <p>{item.description}</p>
                </li>
              </a>
            ))}
          </ul>
        </Repos>
      </main>
    </>
  );
};
