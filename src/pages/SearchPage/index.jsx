import { useHistory } from "react-router-dom";
import { useState } from "react";
import "./style.css";
import SimpleDialog from "@material-ui/core/Dialog";

import {
  SearchResult,
  PreviousPageButton,
  SeeMoreButton,
  ModalMenu,
} from "./style";

export const SearchPage = ({ searchData }) => {
  const searchItems = searchData["items"];

  const history = useHistory();

  const [users, setUsers] = useState(searchItems);
  const [page, setPage] = useState(2);
  const [userModalStatus, setUserModalStatus] = useState(false);
  const [modalInfo, setModalInfo] = useState([]);

  const handleBackToUserPage = () => {
    history.goBack();
  };

  const handleSeeMore = async (currentPage) => {
    const query = searchData.items[0].login;

    const response = await fetch(
      `https://api.github.com/search/users?q=${query}&page=${page}`
    )
      .then((response) => response.json())
      .then((response) => setUsers([...users, ...response["items"]]));
    setPage(currentPage + 1);
    return response;
  };

  const handleModalInfo = (user) => {
    fetch(`https://api.github.com/users/${user}/repos`)
      .then((response) => response.json())
      .then((response) => setModalInfo(response));
  };

  const openModal = async (user) => {
    await handleModalInfo(user);
    setUserModalStatus(true);
  };
  const closeModal = () => {
    setUserModalStatus(false);
    setModalInfo([]);
  };

  return (
    <>
      <header>
        <PreviousPageButton onClick={handleBackToUserPage}>
          voltar
        </PreviousPageButton>
      </header>
      <main>
        <SearchResult>
          {users.map((item, index) => (
            <li key={index} onClick={() => openModal(item.login)}>
              <img src={item["avatar_url"]} alt={item.login} />
              <h3>{item.login}</h3>
            </li>
          ))}
          <SimpleDialog
            className="modal"
            open={userModalStatus}
            onClose={closeModal}
          >
            <>
              <header>
                <ModalMenu>
                  <li>
                    <button disabled className="follow-button">
                      Seguir
                    </button>
                  </li>
                  <li>
                    <button className="profile-button">
                      <a
                        href={
                          modalInfo.length > 0
                            ? modalInfo[0].owner["html_url"]
                            : "/"
                        }
                        target="_blank"
                        rel="noreferrer"
                      >
                        Perfil
                      </a>
                    </button>
                  </li>
                  <li>
                    <button className="close-button" onClick={closeModal}>
                      <span>X</span>
                    </button>
                  </li>
                </ModalMenu>
              </header>
              <h1>Repositorios</h1>
              <ul>
                {modalInfo.map((item, index) => (
                  <li key={index} className="repo">
                    <a href={item["html_url"]} target="_blank" rel="noreferrer">
                      <h3>{item.name}</h3>
                    </a>
                    <p>{item.description}</p>
                    <p>
                      <strong>{item.language}</strong>
                    </p>
                  </li>
                ))}
              </ul>
            </>
          </SimpleDialog>
        </SearchResult>
        <SeeMoreButton className="seemore" onClick={() => handleSeeMore(page)}>
          Ver mais
        </SeeMoreButton>
      </main>
    </>
  );
};
