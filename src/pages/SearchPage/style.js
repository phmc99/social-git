import styled from "styled-components";

export const SearchResult = styled.ul`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 10px;
  margin: 20px 0;

  li {
    background-color: lightgray;
    border-radius: 5px;
    width: 300px;
    height: 55px;
    display: flex;
    align-items: center;
    gap: 15px;
    padding: 5px;

    img {
      width: 40px;
      height: 40px;
      border-radius: 50%;
      box-shadow: 0 0 3px black;
    }

    h3 {
      color: rgb(36, 36, 36);
      font-weight: 500;
    }
  }

  @media (min-width: 768px) {
    max-width: 800px;
    margin: 0 auto;
    padding: 10px;

    flex-direction: row;
    flex-wrap: wrap;

    justify-content: center;

    li {
      width: 200px;
      overflow: hidden;
      text-overflow: ellipsis;

      &:hover {
        cursor: pointer;
        user-select: none;

        transform: scale(105%);
        box-shadow: 0 0 5px gray;
        transition: all 0.2s;

        h3 {
          color: midnightblue;
          transition: all 0.2s;
        }
      }
    }
  }
`;

export const PreviousPageButton = styled.button`
  border: 0;
  padding: 10px;
  border-radius: 10px;
  margin-top: 10px;

  position: fixed;
  left: 95%;
  transform: translate(-95%);

  box-shadow: 0 0 5px black;
  text-transform: uppercase;
  font-weight: 600;

  background-color: #474747;
  color: #f3f3f3;

  @media (min-width: 768px) {
    &:hover {
      filter: brightness(1.15);
      transition: all 0.2s;
    }
  }
`;

export const SeeMoreButton = styled.button`
  border: 0;
  padding: 10px;
  border-radius: 10px;
  width: 120px;

  box-shadow: 0 0 5px black;
  text-transform: uppercase;
  font-weight: 600;

  background-color: #474747;
  color: #f3f3f3;

  @media (min-width: 768px) {
    &:hover {
      filter: brightness(1.15);
      transform: scale(95%);
      transition: all 0.2s;
    }
  }
`;

export const ModalMenu = styled.ul`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  height: 80px;

  li {
    button {
      padding: 10px;
      width: 70px;
      border: 0;
      border-radius: 5px;

      background-color: rgb(36, 36, 36);
      color: #f3f3f3;

      &:hover {
        filter: brightness(1.3);
        transition: all 0.2s;
      }
    }

    .follow-button {
      cursor: not-allowed;
      &:disabled {
        filter: brightness(0.25);
        opacity: 0.85;
      }
    }

    .profile-button {
      a {
        color: #f3f3f3;
        width: 60px;
      }
      &:hover {
        background-color: rgb(16, 16, 16);

        a {
          color: royalblue;
        }
      }
    }

    .close-button {
      background-color: transparent;
      padding: 5px;

      span {
        font-weight: 600;
        color: darkred;
        font-size: 22px;
      }

      &:hover {
        transition: all 0.2s;
        span {
          text-shadow: 0 0 2px #8b0000;
        }
      }
    }
  }
`;
