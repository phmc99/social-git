import styled from "styled-components";

export const Modal = styled.div`
  position: fixed;
  background-color: #f3f3f3;
  width: 100%;
  height: 100vh;

  .content {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);

    width: 300px;
    padding: 10px;
    background-color: #242424;
    border-radius: 5px;
    gap: 10px;

    display: flex;
    flex-direction: column;
    align-items: center;

    h1 {
      font-size: 24px;
      color: #f3f3f3;
    }

    input {
      border: 0;
      padding: 8px;
      width: 240px;
      border-radius: 5px;

      background-color: #cdcdcd4f;
      color: #f3f3f3;
    }

    button {
      border: 0;
      padding: 8px;
      width: 150px;
      border-radius: 5px;

      font-size: 14px;
      font-weight: 600;

      color: rgb(36, 36, 36);
      background-color: #cdcdcd;
    }
  }

  @media (min-width: 768px) {
    .content {
      width: 500px;
      height: 250px;

      justify-content: center;

      h1 {
        font-size: 36px;
      }

      input {
        padding: 12px;
      }

      button {
        font-size: 16px;

        &:hover {
          filter: brightness(1.25);
          transition: all 0.5s;
        }
      }
    }
  }
`;
