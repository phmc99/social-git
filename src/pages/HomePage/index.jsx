import { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Modal } from "./style";
import toast from "react-hot-toast";

export const HomePage = ({ setData }) => {
  const [userInput, setUserInput] = useState("");
  const history = useHistory();

  useEffect(() => {
    autoLogin();
  });

  const autoLogin = async () => {
    const user = localStorage.getItem("user");
    if (user !== null) {
      await fetch(`https://api.github.com/users/${user}/repos`)
        .then((response) => response.json())
        .then((response) => {
          setData(response);
        });
      history.push("/user");
    }
  };

  const handleSubmit = async (input) => {
    if (input.trim() === "") {
      toast.error("Insira seu nome de úsuario do GitHub", {
        duration: 1000,
      });
    } else {
      const response = await fetch(
        `https://api.github.com/users/${input}/repos`
      )
        .then((response) => response.json())
        .then((response) => {
          if (response.length === 0 || response.message === "Not Found") {
            toast.error("Usuário inválido");
          } else {
            setData(response);
            localStorage.setItem("user", input);
            toast.success("Bem vindo!", { duration: 2000 });
            history.push("/user");
          }
        });

      return response;
    }
  };

  return (
    <Modal>
      <div className="content">
        <h1>Entre com seu GitHub</h1>
        <input
          type="text"
          value={userInput}
          onChange={(event) => setUserInput(event.target.value)}
          placeholder="Digite seu úsuario do GitHub"
        />
        <button onClick={() => handleSubmit(userInput)}>Entrar</button>
      </div>
    </Modal>
  );
};
