import "./styles/reset.css";
import { useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import { UserProfile } from "./pages/UserPage";
import { HomePage } from "./pages/HomePage";
import { SearchPage } from "./pages/SearchPage";

import { Toaster } from "react-hot-toast";

export const App = () => {
  const [userData, setUserData] = useState([]);
  const [searchResult, setSearchResult] = useState([]);

  return (
    <Router>
      <Toaster />
      <Switch>
        <Route exact path="/">
          <HomePage setData={setUserData} />
        </Route>

        <Route exact path="/user">
          <UserProfile
            userData={userData}
            searchResult={searchResult}
            setSearchResult={setSearchResult}
          />
        </Route>

        <Route exact path="/search">
          <SearchPage searchData={searchResult} />
        </Route>
      </Switch>
    </Router>
  );
};
