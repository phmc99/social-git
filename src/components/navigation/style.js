import styled from "styled-components";

export const Menu = styled.ul`
  display: flex;
  justify-content: space-between;
  list-style: none;

  background-color: rgb(36, 36, 36);
  padding: 20px;

  li {
    text-transform: lowercase;
    color: #fff;
  }

  li > input {
    padding: 8px;
    width: 120px;
    border: 0;

    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;

    background-color: #cdcdcd4f;
    color: #f3f3f3;
  }

  li > button {
    width: 60px;
    padding: 8px;
    border: 0;

    border-bottom-right-radius: 5px;
    border-top-right-radius: 5px;

    color: rgb(36, 36, 36);
    background-color: #cdcdcd;
  }

  .logout {
    border-radius: 5px;
    background-color: #d79797;
  }

  @media (min-width: 768px) {
    li > input {
      width: 300px;
      padding: 10px;
    }
    li > button {
      padding: 10px;

      &:hover {
        transition: all 0.5s;
        filter: brightness(1.25);
      }
    }
  }

  @media (min-width: 1440px) {
    justify-content: space-evenly;
  }
`;
