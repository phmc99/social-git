import { useState } from "react";
import { Menu } from "./style";
import { useHistory } from "react-router-dom";
import toast from "react-hot-toast";

export const NavBar = ({ setSearchResult }) => {
  const [userInput, setUserInput] = useState("");
  const history = useHistory();

  const handleSubmit = async (input) => {
    if (input.trim() === "") {
      toast.error("Insira o nome do úsuario", {
        duration: 1000,
      });
    } else {
      const response = await fetch(
        `https://api.github.com/search/users?q=${input}&page=1`
      )
        .then((response) => response.json())
        .then((response) => setSearchResult(response));

      history.push("/search");
      return response;
    }
  };

  const handleLogOut = () => {
    toast("Até logo!", {
      icon: "👋",
      duration: 2000,
    });
    localStorage.removeItem("user");
    history.push("/");
  };

  return (
    <Menu>
      <li>
        <button className="logout" onClick={handleLogOut}>
          Sair
        </button>
      </li>
      <li key={1}>
        <input
          type="text"
          value={userInput}
          onChange={(event) => setUserInput(event.target.value)}
          placeholder="Pesquise o úsuario"
        />
        <button onClick={() => handleSubmit(userInput)}>Buscar</button>
      </li>
    </Menu>
  );
};
